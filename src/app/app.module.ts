import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonListComponent as PokemonList} from './component/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent as PokemonListItem} from './component/pokemon-list-item/pokemon-list-item.component';
import { PokemonDetailComponent  as PokemonDetail} from './component/pokemon-detail/pokemon-detail.component';
import { PokemonProfileComponent as PokemonProfile} from './component/pokemon-profile/pokemon-profile.component';
import { PokemonStatsComponent as PokemonStats} from './component/pokemon-stats/pokemon-stats.component';
import { PokemonMovesComponent as PokemonMoves} from './component/pokemon-moves/pokemon-moves.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    PokemonList,
    PokemonListItem,
    PokemonDetail,
    PokemonProfile,
    PokemonStats,
    PokemonMoves
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: 'pokeDex', component: PokemonList},
      { path: 'pokemonDetail/:id', component: PokemonDetail},
      /*{ path: '', component: PokemonProfile},
      { path: '', component: PokemonStats},
      { path: '', component: PokemonMoves},*/
      { path: '', pathMatch: 'full', redirectTo: 'pokeDex'},
      { path: '**', redirectTo: '404NotFound'}
    ]),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
