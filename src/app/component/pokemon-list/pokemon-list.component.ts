import { Component, OnInit } from '@angular/core';
import { PokemonDataService } from '../../pokemon-data.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  pokemons = [];
  nextTwenty: string;

  constructor(private pokemonDataService: PokemonDataService) { }

  async ngOnInit() {
    try {
      const data: any = await this.pokemonDataService.sendGetRequest("https://pokeapi.co/api/v2/pokemon");
      this.pokemons = data.results;
      this.nextTwenty = data.next;


    } catch (err) { console.log(err) }

    for (let p of this.pokemons) {
      this.getPokemonData(p);
    }
  }



  getPokemonData(pokemon: any) {
    this.pokemonDataService.getPokemonByUrl(pokemon.url).subscribe(a => {
      pokemon.image = a.sprites.front_default;
    }, err => {
      console.log(err);
    });
  }

  public getPokemons() {
    return this.pokemons;
  }

  public loadMorePokemon() {
    this.pokemonDataService.getPokemonByUrl(this.nextTwenty).subscribe(a => {
      this.pokemons.push(...a.results);
      this.nextTwenty = a.next;
      for (let i=this.pokemons.length-20; i<this.pokemons.length; i++) {
        this.getPokemonData(this.pokemons[i]);
      }
    }, err => {
      console.log(err);
    });
  }
}