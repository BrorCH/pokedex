import { Component, OnInit, Input } from '@angular/core';
import { PokemonDataService } from '../../pokemon-data.service';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {

  pokemonInfo = [];

  constructor(private PokemonDataService: PokemonDataService) {
    let urlString = window.location.pathname.split("/");
    let name = urlString[urlString.length - 1];
    console.log(name);
    this.PokemonDataService.getPokemonByName(name).subscribe(a => {
      this.pokemonInfo = a;
    }, err => {
      console.log(err);
    });

  }

  ngOnInit() {
  }

}
