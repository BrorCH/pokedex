import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonDataService {

  httpUrl = "https://pokeapi.co/api/v2/pokemon";

  constructor(private httpClient: HttpClient) { }

  public sendGetRequest(url: string) {
    return this.httpClient.get<any>(url).toPromise();
  }

  getAllPokemon(){

  }

  getPokemonByUrl(url:string){
    return this.httpClient.get<any>(url);
  }

  getPokemonByName(name:string){
    return this.httpClient.get<any>(this.httpUrl+"/"+name);
  }
}
